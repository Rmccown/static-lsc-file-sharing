# StaticLSCFileSharing

StaticLSCFileSharing is a plugin to keep SocialEngine FileSharing plugin paths the same for Discourse.

It has no real value outside of LSC, though some of the pictures are cool.

It also shows how to load a bunch of images via a plugin

## Installation

Install as you do other plugins, then rebuid Discourse.

You will need to symbolically link the FileSharing directory after the Discourse build.

From the container's public directory:

`ln -s plugins/StaticLSCFileSharing/FileSharing ./FileSharing`
